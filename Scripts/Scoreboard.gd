extends Node

export var P1ScoreNode = NodePath()
export var P2ScoreNode = NodePath()

var P1ScoreText = null
var P2ScoreText = null

var player1Score = 0
var player2Score = 0

# Called when the node enters the scene tree for the first time.
func _ready():
	P1ScoreText = get_node(P1ScoreNode)
	P2ScoreText = get_node(P2ScoreNode)

	ResetScore()
	UpdateBoards()
	pass

func UpdateScore(isP1):
	if isP1 == true:
		player1Score += 1
	else:
		player2Score += 1

	UpdateBoards()

func ResetScore():
	player1Score = 0
	player2Score = 0
	UpdateBoards()

func UpdateBoards():
	P1ScoreText.bbcode_text = "[right]" + str(player1Score) + "[/right]"
	P2ScoreText.text = str(player2Score)
