extends KinematicBody2D

export var startLoc = NodePath()
export var p1ScoreZone = NodePath()
export var p2ScoreZone = NodePath()
export var scoreBoardPath = NodePath()

export var baseSpeed = 200;
export var speedIncreaseOnBounce = .05

var curSpeed = baseSpeed

var startNode = null

var p1Scoring = null
var p2Scoring = null
var scoreBoard = null

var moveDir = Vector2(-1, -1)

var rng = null

# Called when the node enters the scene tree for the first time.
func _ready():
	p1Scoring = get_node(p1ScoreZone)
	p2Scoring = get_node(p2ScoreZone)
	scoreBoard = get_node(scoreBoardPath)

	rng = RandomNumberGenerator.new()
	rng.randomize()

	ResetPos()

	moveDir.y = rng.randf_range(-1.0, 1.0)

	pass

func _physics_process(delta):
	var collision = self.move_and_collide(moveDir * curSpeed * delta)
	if collision == null:
		return
	
	print(collision.collider_id)

	if collision.collider_id == p1Scoring.get_instance_id():
		scoreBoard.UpdateScore(true)
		ResetPos()

		moveDir = Vector2(-1, rng.randf_range(-1.0, 1.0))
		return
	elif collision.collider_id == p2Scoring.get_instance_id():
		scoreBoard.UpdateScore(false)
		ResetPos()

		moveDir = Vector2(1, rng.randf_range(-1.0, 1.0))
		return

	curSpeed += speedIncreaseOnBounce

	var collisionPos = collision.position
	var collisionNormal = collision.normal
	var colliderVelocity = collision.collider_velocity

	# print("Pos: ", collisionPos, " | Normal: ", collisionNormal, " | Velocity: ", colliderVelocity)

	if collisionNormal.x > .1 or collisionNormal.x < -.1:
		moveDir.x = collisionNormal.x
	if collisionNormal.y > .1 or collisionNormal.y < -.1:
		moveDir.y = collisionNormal.y
		
	# Clamp velocity so it moves no matter what
	if moveDir.x > -.1 and moveDir.x < .1:
		if moveDir.x > 0:
			moveDir.x = .1
		else:
			moveDir.x = -.1

# Called every frame. 'delta' is the elapsed time since the previous frame.
func _process(delta):
	pass

func ResetPos():
	if startNode == null:
		startNode = get_node(startLoc)
	position = startNode.position
	curSpeed = baseSpeed

