extends KinematicBody2D

class_name Paddle

# Export makes it editable in the inspector
export var speed = 200
export var player1 = true;

# Called when the node enters the scene tree for the first time.
func _ready():
	pass # Replace with function body.

func _physics_process(delta):
	var moveDir = Vector2.ZERO

	var upKey = KEY_W
	var downKey = KEY_S

	if player1 == false:
		upKey = KEY_UP
		downKey = KEY_DOWN

	if Input.is_key_pressed(upKey):
		moveDir = Vector2.UP
	elif Input.is_key_pressed(downKey):
		moveDir = Vector2.DOWN

	var _coll = self.move_and_collide(moveDir * speed * delta);

# Called every frame. 'delta' is the elapsed time since the previous frame.
func _process(delta):
	pass
	"""
	var moveDir = Vector2.ZERO

	var upKey = KEY_W
	var downKey = KEY_S

	if player1 == false:
		upKey = KEY_UP
		downKey = KEY_DOWN

	if Input.is_key_pressed(upKey):
		moveDir = Vector2.UP
	elif Input.is_key_pressed(downKey):
		moveDir = Vector2.DOWN

	var _coll = self.move_and_collide(moveDir * speed * delta);
	"""
